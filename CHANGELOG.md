
## 1.1.3 [06-29-2020]

* Update MAINTAINERS

See merge request itentialopensource/itentialpromisify!14

---
## 1.1.2 [06-11-2019]
* [patch/reademeFix] Fix readme so that the npm page displays the correct command to install the node module

See merge request itentialopensource/itentialpromisify!13

---

## 1.1.1 [06-10-2019]
* [Patch/context fix] Promisify pushes context through to modified function

See merge request itentialopensource/itentialpromisify!12

---

## 1.1.0 [05-30-2019]
* adds a feature- the ability to promisify single functions instead of a whole...

See merge request itentialopensource/itentialpromisify!10

---

## 1.0.4 [05-23-2019]
* :memo: :white_check_mark: Adds comments and checks that property retrieved is of type function.

See merge request itentialopensource/itentialpromisify!9

---

## 1.0.3 [05-22-2019]
* Patch/cleanup readme

See merge request itentialopensource/itentialpromisify!8

---

## 1.0.2 [05-22-2019]
* :wrench: Fix incorrect contact info

See merge request itential/delivery-support/itentialpromisify!5

---

## 1.0.1 [05-22-2019]
* Prepare the project for open source

See merge request itential/delivery-support/itentialpromisify!4

---
