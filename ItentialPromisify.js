const { promisify } = require('util');

/**
 * @summary The intent of this function is to enable a developer to use async/await
 * for any Itential cog or adapter methods. Itential uses an error second callback convention
 * and so normal promisify will not work for Itential cog or adapter methods.
 * @param { * } itemToPromisify The cog, adapter, other itential object, or function to promisify
 * @returns { Proxy } proxyForObject A new object in memory that references the original
 * cog or adapter with the function to invoke so that a promise is returned to use with async/await
 */
function itentialPromisify(itemToPromisify) {
  if (typeof itemToPromisify === 'object') {
  // Use a proxy to invoke the get trap defined below rather than the original cog or adapter
    return new Proxy(itemToPromisify, {
      // When invoking a function that is a property of an object
      // (like when invoking cogs.WorkFlowEngine.startJobWithOptions(workflowName, options)),
      // the first step is to get the startJobWithOptions property from the
      // cogs.WorkFlowEngine object. Therefore, a handler.get() trap must be defined for this
      // function
      // (see:
      // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy
      // for reference)

      // In the function below, target is a reference to the original object, which has the
      // function we want to call. The propertyKey is the property we are referencing;
      // in this context the function we want to call.
      get(target, propertyKey) {
        const originalMethod = target[propertyKey];

        // If the property retrieved is something other than a function, return it to the caller
        if (typeof originalMethod !== 'function') {
          return originalMethod;
        }

        // Because there are a variable number of arguments for the promisified function,
        // use the spread operator (...) and a variable name as a reference to the list of
        // arguments provided
        // (see:
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax)
        return (...callingArguments) => new Promise((resolve, reject) => {
        // The business logic of reversing the order of the result and
        // error for the callback.
          callingArguments.push((result, error) => {
            if (error) return reject(error);
            return resolve(result);
          });

          // Call the property retrieved from the target with the modified arguments with callback
          originalMethod.apply(itemToPromisify, callingArguments);
        });
      },
    });

  // If the item the user wants to promisify is just a function then we can flip the order
  // of arguments to the callback and then use Node's native promisify function on it.
  }
  if (typeof itemToPromisify === 'function') {
    const originalFunctionName = itemToPromisify.name;

    // flipCallback is a function that takes in a callback and returns a function
    // that calls callback with the arguments reversed.
    const flipCallback = callback => (error, result) => callback(result, error);

    // Create a function that calls flipCallback on the last argument. When this happens
    // the arguments to the callback will be in the order that node.util promisify
    // expects- i.e. error, result. The last thing this function does is return a
    // call to the original function with the reversed callback.
    const functionThatFlipsCallback = function functionThatFlipsCallback(...args) {
      const numArgs = args.length;
      const callback = flipCallback(args[numArgs - 1]);
      return itemToPromisify.call(this, ...args.slice(0, numArgs - 1), callback);
    };
    Object.defineProperty(functionThatFlipsCallback, 'name', { value: originalFunctionName });

    // Return the result of util.promisify on the function we just created that flips callback
    // arugment order. Since the returned function is promsified, it can be called without
    // the last argument. Promisify will push the last argument (the callback) into the args
    // array and our function will handle flipping it so the error/result order is reversed.
    return promisify(functionThatFlipsCallback);
  }
  throw new TypeError('itentialPromisify must be used with an object or function.');
}

module.exports = itentialPromisify;
