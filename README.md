# ItentialPromisify

ItentialPromisify wraps cog and adapter functions to enable using async/await with them.

You can also use ItentialPromisify to promisify a single function that expects a callback with arguments in the order results, error.

## Installation

`npm install --save @itentialopensource/itentialpromisify`

## Usage with cogs/adapters

1. Require `ItentialPromisfy`.

    ```javascript
    const itentialPromisify = require('@itentialopensource/itentialPromisify');
    ```

2. Wrap an `adapter` or `cog`.

    ```javascript
    const workFlowEngineWrapper = itentialPromisify(cogs.WorkFlowEngine);
    ```

3. Use `async`/`await` with any method from the cog you wrapped.

    ```javascript
      try {
        const workflowName = 'routerUpgrade';
        const options = {
          variables: {
            new_version: '1.8.8'
          }
        };
        // N.b. no need to pass a callback to the wrapper!
        const saveData = await workFlowEngineWrapper.startJobWithOptions(workflowName, options);
        console.log(JSON.stringify(saveData, null, 2));
      } catch (error) {
        console.log(error.stack);
      }
    ```

## Usage with single functions

1. Require `ItentialPromisfy`.

    ```javascript
    const itentialPromisify = require('@itentialopensource/itentialPromisify');
    ```

2. Call itentialPromisify on the function

    ```javascript
    const startJobWithOptionsPromisified = itentialPromisify(cogs.WorkFlowEngine.startJobWithOptions);
    ```

3. Use `async`/`await` with the promisified method.

    ```javascript
        try {
          const workflowName = 'routerUpgrade';
          const options = {
            variables: {
              new_version: '1.8.8'
            }
          };
          const saveData = await startJobWithOptionsPromisified(workflowName, options);
          console.log(JSON.stringify(saveData, null, 2));
        } catch (error) {
          console.log(error.stack);
        }
    ```

## :open_hands: Contributing to itentialPromisify

Please check out the [Contributing Guidelines](./CONTRIBUTING.md).

License & Maintainers
---

### Maintained by:

Itential Cloud & Delivery Support Teams

### License

[Apache 2.0](./LICENSE)