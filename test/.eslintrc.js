module.exports = {
  "env": {
    "mocha": true
  },
  'rules': {
    // mocha discourages the use of lambda functions 😢
    // https://mochajs.org/#arrow-functions
    'func-names': 0,
    'prefer-arrow-callback': 0,
  },
};