/* eslint-disable max-len */

const { expect } = require('chai');
const itentialPromisify = require('../ItentialPromisify');

const adapters = {
  servicenow: {
    id: 0,
    getSystemDetails: (sysId, osType, callback) => callback(`showing system details for ${sysId} with os ${osType}`),
    getSystemDetailsError: (sysId, osType, callback) => callback(null, new Error(`Error showing system details for ${sysId} with os ${osType}`)),
  },
};

const cogs = {
  WorkflowEngine: {
    startJobWithOptions: (workflow, options, callback) => callback(`started ${workflow} with options ${JSON.stringify(options, null, 2)}`),
    startJobWithOptionsError: (workflow, options, callback) => callback(null, new Error(`Error starting ${workflow} with options ${JSON.stringify(options, null, 2)}`)),
  },
};

describe('Itential Promisify tests', function () {
  it('should resolve with a result when an adapater function does not error', async () => {
    const servicenow = itentialPromisify(adapters.servicenow);
    const sysId = 'abcd1291d';
    const osType = 'linux';
    try {
      const results = await servicenow.getSystemDetails(sysId, osType);
      expect(results).to.equal(`showing system details for ${sysId} with os ${osType}`);
    } catch (error) {
      expect(error).to.deep.equal(undefined);
    }
  });
  it('should catch an error if the adapter calls back with one', async () => {
    const servicenow = itentialPromisify(adapters.servicenow);
    const sysId = 'abcd1291d';
    const osType = 'linux';
    try {
      await servicenow.getSystemDetails(sysId, osType);
    } catch (error) {
      expect(error.message).to.equal(`Error showing system details for ${sysId} with os ${osType}`);
    }
  });
  it('should return a requested property that is not a function', async () => {
    const servicenow = itentialPromisify(adapters.servicenow);
    const { id } = servicenow;
    expect(id).to.equal(0);
  });
  it('should resolve with a result when an cog function does not error', async () => {
    const workflowEngine = itentialPromisify(cogs.WorkflowEngine);
    const workflow = 'Router software update';
    const options = {
      variables: {
        oldVersion: '1.4.15',
        newVersion: '1.8.1',
      },
    };
    try {
      const results = await workflowEngine.startJobWithOptions(workflow, options);
      expect(results).to.equal(`started ${workflow} with options ${JSON.stringify(options, null, 2)}`);
    } catch (error) {
      expect(error).to.deep.equal(undefined);
    }
  });
  it('should catch the error if the cog function call back with an error', async () => {
    const workflowEngine = itentialPromisify(cogs.WorkflowEngine);
    const workflow = 'Router software update';
    const options = {
      variables: {
        oldVersion: '1.4.15',
        newVersion: '1.8.1',
      },
    };
    try {
      await workflowEngine.startJobWithOptionsError(workflow, options);
    } catch (error) {
      expect(error.message).to.equal(`Error starting ${workflow} with options ${JSON.stringify(options, null, 2)}`);
    }
  });
  it('should work on a single function and await the result to resolve', async () => {
    const startJobWithOptionsPromsified = itentialPromisify(cogs.WorkflowEngine.startJobWithOptions);
    const workflow = 'Router software update';
    const options = {
      variables: {
        oldVersion: '1.4.15',
        newVersion: '1.8.1',
      },
    };
    try {
      const results = await startJobWithOptionsPromsified(workflow, options);
      expect(results).to.equal(`started ${workflow} with options ${JSON.stringify(options, null, 2)}`);
    } catch (error) {
      expect(error).to.deep.equal(undefined);
    }
  });
  it('should work on a single function a catch an error', async () => {
    const startJobWithOptionsPromsified = itentialPromisify(cogs.WorkflowEngine.startJobWithOptionsError);
    const workflow = 'Router software update';
    const options = {
      variables: {
        oldVersion: '1.4.15',
        newVersion: '1.8.1',
      },
    };
    try {
      await startJobWithOptionsPromsified(workflow, options);
    } catch (error) {
      expect(error.message).equal(`Error starting ${workflow} with options ${JSON.stringify(options, null, 2)}`);
    }
  });
  it('should throw a TypeError if you try and use itentialPromisify on something that is not a function or object', async () => {
    const expectedErrorMessage = 'itentialPromisify must be used with an object or function.';
    expect(() => {
      itentialPromisify('tryAndPromisifyAString');
    }).to.throw(TypeError, expectedErrorMessage);
  });
  it('should send the context of the calldown into the flippedCallbackFunction', async () => {
    class PromisifyContextTestClass {
      constructor() {
        this.name = 'test';
      }

      replace(callback) {
        return callback(this.name);
      }
    }

    const TestClass = new PromisifyContextTestClass();
    TestClass.replace = await itentialPromisify(TestClass.replace);
    const thisName = await TestClass.replace();
    expect(thisName).to.equal('test');
  });
});
